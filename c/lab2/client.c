#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#define MAX 1024
#define PORT 3000
#define SOCK_ADDR "localhost"

void communicate(int sockfd) {
    char msg[MAX];
    int n;

    printf("[B/H/O][Number]\n");
    printf("or input [FILE]\n");

    while (1) {
        bzero(msg, sizeof(msg));
        printf("Enter command: ");
        n = 0;

        while ((msg[n++] = getchar()) != '\n');

        if (send(sockfd, msg, strlen(msg), 0) < 0) {
            perror("Error in send(): ");
            return;
        }

        if ((strncmp(msg, "exit", 4)) == 0) {
            printf("Client Exit...\n");
            return;
        }

        bzero(msg, sizeof(msg));
        recv(sockfd, msg, sizeof(msg), 0);

        printf("From Server :\n%s\n", msg);
    }
}

int main(void) {
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("Socket error");
        return EXIT_FAILURE;
    }

    struct hostent *host = gethostbyname(SOCK_ADDR);
    if (!host) {
        perror("Host name error");
        return EXIT_FAILURE;
    }

    struct sockaddr_in server_addr;
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    server_addr.sin_addr = *((struct in_addr *) host->h_addr_list[0]);

    if (connect(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        perror("Connect error");
        return EXIT_FAILURE;
    }

    communicate(sockfd);
    close(sockfd);
    return EXIT_SUCCESS;
}
