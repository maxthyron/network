#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <strings.h>

#define MAX_CONNECTIONS 5

#define MAX 1024
#define PORT 3000
#define SOCK_ADDR "localhost"

#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

void parse(char *buff, char *type, int *index) {
    int i = 0;
    int current;

    if ((strncmp(buff, "FILE", 4) == 0 || strncmp(buff, "file", 4) == 0)) {
        *type = 'F';
        return;
    }

    if (buff[0] == 'B' || buff[0] == 'H' || buff[0] == 'O' || buff[0] == 'b' || buff[0] == 'h' || buff[0] == 'o') {
        *type = buff[0];
    } else {
        printf("Error! Wrong command!");
        *type = 'E';
        return;
    }

    buff++;
    while (*buff != '\n') {
        current = *buff - '0';

        if (current > 9 || current < 0) {
            printf("Error! Wrong command!");
            *index = -1;
            return;
        }

        i = i * 10 + current;
        buff++;
    }
    *index = i;
    printf("Type = %c Index = %d\n", *type, *index);
}

void getData(char *buff, char type, int index) {
    FILE *fp = fopen("address.txt", "r");
    int b1, b2, b3, b4;

    if (type == 'F') {
        char current[MAX];
        while (fgets(current, MAX, fp) != NULL) {
            buff = strcat(buff, current);
        }
    } else if (type != 'E') {
        for (int i = 0; i < index + 1; i++) {
            fscanf(fp, "%d.%d.%d.%d\n", &b1, &b2, &b3, &b4);
        }
        if (type == 'B') {
            sprintf(buff,
                    "%c%c%c%c%c%c%c%c.%c%c%c%c%c%c%c%c.%c%c%c%c%c%c%c%c.%c%c%c%c%c%c%c%c\n",
                    BYTE_TO_BINARY(b1),
                    BYTE_TO_BINARY(b2),
                    BYTE_TO_BINARY(b3),
                    BYTE_TO_BINARY(b4));
        } else if (type == 'H') {
            sprintf(buff, "%x.%x.%x.%x\n", b1, b2, b3, b4);
        } else if (type == 'O') {
            sprintf(buff, "%o.%o.%o.%o\n", b1, b2, b3, b4);
        } else if (type == 'b') {
            FILE *fp1 = fopen("address.txt", "r");
            char temp[MAX];
            while (fgets(temp, MAX, fp1) != NULL) {
                sscanf(temp, "%d.%d.%d.%d\n", &b1, &b2, &b3, &b4);
                sprintf(temp,
                        "%c%c%c%c%c%c%c%c.%c%c%c%c%c%c%c%c.%c%c%c%c%c%c%c%c.%c%c%c%c%c%c%c%c\n",
                        BYTE_TO_BINARY(b1),
                        BYTE_TO_BINARY(b2),
                        BYTE_TO_BINARY(b3),
                        BYTE_TO_BINARY(b4));
                buff = strcat(buff, temp);
            }
        } else if (type == 'h') {
            FILE *fp1 = fopen("address.txt", "r");
            char temp[MAX];
            while (fgets(temp, MAX, fp1) != NULL) {
                sscanf(temp, "%d.%d.%d.%d\n", &b1, &b2, &b3, &b4);
                sprintf(temp, "%x.%x.%x.%x\n", b1, b2, b3, b4);
                buff = strcat(buff, temp);
            }
        } else if (type == 'o') {
            FILE *fp1 = fopen("address.txt", "r");
            char temp[MAX];
            while (fgets(temp, MAX, fp1) != NULL) {
                sscanf(temp, "%d.%d.%d.%d\n", &b1, &b2, &b3, &b4);
                sprintf(temp, "%o.%o.%o.%o\n", b1, b2, b3, b4);
                buff = strcat(buff, temp);
            }
        }

    }

    fclose(fp);
}

void communicate(int sockfd) {
    char buff[MAX];
    int index;
    char type;

    while (1) {
        bzero(buff, MAX);
        recv(sockfd, buff, sizeof(buff), 0);

        printf("From client: %sTo client  : \n", buff);

        if (strncmp("exit", buff, 4) == 0) {
            printf("Server Exit...\n");
            break;
        }

        parse(buff, &type, &index);

        if (type == 'E' || (index == -1 && type != 'F')) {
            sprintf(buff, "Error, wrong command!");
            if (send(sockfd, buff, strlen(buff), 0) < 0) {
                perror("Error in send(): ");
                return;
            }
        }

        bzero(buff, MAX);
        getData(buff, type, index);
        printf("%s", buff);

        if (send(sockfd, buff, strlen(buff), 0) < 0) {
            perror("Error in send(): ");
            return;
        }
    }
}

int main() {
    int sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd < 0) {
        perror("Socket error");
        return EXIT_FAILURE;
    }

    struct sockaddr_in l_addr;

    bzero(&l_addr, sizeof(l_addr));

    l_addr.sin_family = AF_INET;         /* host byte order */
    l_addr.sin_port = htons(PORT);       /* short, network byte order */
    l_addr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */
    bzero(&(l_addr.sin_zero), 8);

    if (bind(sockfd, (struct sockaddr *) &l_addr, sizeof(struct sockaddr)) == -1) {
        perror("Bind error");
        return EXIT_FAILURE;
    }

    if (listen(sockfd, MAX_CONNECTIONS) == -1) {
        perror("Listen error");
        return EXIT_FAILURE;
    }

    struct sockaddr_in client_addr;
    int addr_size = sizeof(client_addr);

    int connfd = accept(sockfd, (struct sockaddr *) &client_addr, (socklen_t *) &addr_size);
    if (connfd < 0) {
        perror("Accept error");
        return EXIT_FAILURE;
    }

    communicate(connfd);
    close(sockfd);
    return EXIT_SUCCESS;
}