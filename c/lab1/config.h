#ifndef CONFIG_H
#define CONFIG_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <signal.h>
#include <limits.h>

#define MSG_SIZE 50
#define PORT 6780

int sock;

void handler(int event);

#endif // CONFIG_H