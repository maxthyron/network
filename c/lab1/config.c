#include "config.h"

void handler(int event)
{
    printf("\nClosing socket: Ctrl+C interruption - %d.\n", event);
    close(sock);
    exit(EXIT_SUCCESS);
}
