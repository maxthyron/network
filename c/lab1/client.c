#include "config.h"

int main() {
    int sock;
    struct sockaddr_in server;
    char message[MSG_SIZE];

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        perror("Socket is not opened");
        return EXIT_FAILURE;
    }

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);

    signal(SIGINT, handler);

    for (;;) {
        printf("Input your message> ");
        if (fgets(message, MSG_SIZE, stdin) == NULL) {
            perror("Message input error");
            return EXIT_FAILURE;
        }

        sendto(sock, message, sizeof(message), 0, (struct sockaddr *) &server, sizeof(server));
    }

    close(sock);
    return EXIT_SUCCESS;
}