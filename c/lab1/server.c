#include "config.h"

int main()
{
    int rval;
    socklen_t len;
    char message[MSG_SIZE];
    struct sockaddr_in server, client;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        perror("Socket error");
        return EXIT_FAILURE;
    }

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);

    if (bind(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        perror("Bind error");
        close(sock);
        return EXIT_FAILURE;
    }

    signal(SIGINT, handler);
    int max_len = INT_MIN;
    int min_len = INT_MAX;

    for (;;)
    {
        rval = recvfrom(sock, message, sizeof(message), 0, (struct sockaddr *)&client, &len);

        message[rval] = 0;
        int message_len = strlen(message) - 1;
        printf("--> %s (%d)\n\n", message, message_len);

        if (message_len > max_len) {
            max_len = message_len;
        }
        if (message_len < min_len) {
            min_len = message_len;
        }

        printf("Max length: %d  Min length: %d\n\n", max_len, min_len);
    }

    return EXIT_SUCCESS;
}
