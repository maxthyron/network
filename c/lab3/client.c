#include "config.h"

int main() {
    int sock;
    struct sockaddr_un server;
    char message[MSG_SIZE];

    sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock < 0) {
        perror("Socket is not opened");
        return EXIT_FAILURE;
    }

    signal(SIGINT, handler);

    for (;;) {
        server.sun_family = AF_UNIX;
        strcpy(server.sun_path, SOCKET_FILE);

        printf("Input your message> ");
        if (fgets(message, MSG_SIZE, stdin) == NULL) {
            perror("Message input error");
            return EXIT_FAILURE;
        }

        sendto(sock, message, sizeof(message), 0, (struct sockaddr *) &server, sizeof(server));
    }

    close(sock);
    return EXIT_SUCCESS;
}