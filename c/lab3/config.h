#ifndef CONFIG_H
#define CONFIG_H

int sock;

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <limits.h>

#define MSG_SIZE 50
#define SOCKET_FILE "socket"

void handler(int event);

#endif // CONFIG_H