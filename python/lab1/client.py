import socket
from config import configs


def main():
    server_address = (configs.address, configs.port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    try:
        while True:
            message = input('Enter your message> ')

            print(f'sending {message}')
            sent = sock.sendto(message.encode("UTF-8"), server_address)

            print('waiting to receive')
            data, server = sock.recvfrom(4096)
            print('received {!r}'.format(data))

    except KeyboardInterrupt:
        print('\nclosing socket')
        sock.close()


if __name__ == "__main__":
    main()
