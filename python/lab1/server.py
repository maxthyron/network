import socket
from config import configs
import math


def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    server_address = (configs.address, configs.port)
    print(f'starting up on {server_address}')
    sock.bind(server_address)
    min_len, max_len = math.inf, -math.inf

    while True:
        print('\nwaiting to receive message')
        data, address = sock.recvfrom(1024)
        message = data.decode('UTF-8')
        message_length = len(message)
        if message_length > max_len:
            max_len = message_length
        if message_length < min_len:
            min_len = message_length

        print(f'received {len(data)} bytes from {address}')
        print(data.decode("UTF-8"))
        print()
        print(f"Min len: {min_len}  Max len: {max_len}")

        if data:
            sent = sock.sendto(data, address)
            print(f'sent {sent} bytes back to {address}')


if __name__ == "__main__":
    main()
